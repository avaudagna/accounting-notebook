

const TRANSACTION_TYPES = {
    DEBIT: 'DEBIT',
    CREDIT: 'CREDIT'
};

export default TRANSACTION_TYPES;