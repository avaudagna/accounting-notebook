import React, { useState, useEffect } from 'react';
import transactionsService from './api/transactions/transactionsService.js';
import TransactionList from './components/TransactionList.js';
import { makeStyles } from '@material-ui/core/styles';
import { grey } from '@material-ui/core/colors';
import TransactionForm from './components/TransactionForm.js';
import AccoutBalance from "./components/AccountBalance";


const useStyles = makeStyles(() => ({
  root: {
      background: grey[300],
      minHeight: '100vh',
      padding: '24px'
  },
  content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
  },
  balance: {
      margin: 'auto auto 16px auto',
      minHeight: '48px',
      width: '80%',
      display: 'flex',
      alignItems: 'center',
  }
}));

const App = () => {
  const [transactions, setTransactions] = useState([]);
  const [balance, setBalance] = useState(0);
  const [submitting, setSubmitting] = useState(false);
  const [submissionErrors, setSubmissionErrors] = useState({});
  const classes = useStyles();
  
  useEffect(() => {
    refreshTransactions();
  }, []);

  const refreshTransactions = () => {
    transactionsService.getTransactions()
      .then((response) => {
          setTransactions(response.data.transactions);
          setBalance(response.data.balance)
      })
  };

  const handleTransactionSubmit = (formData) => {
    transactionsService.submit(formData)
    .then(() => { refreshTransactions(); setSubmissionErrors({}); })
    .catch((error) => setSubmissionErrors({amount: error.response.data}))
    .finally(() => {
      setSubmitting(false)
    })
  };


  return (
    <div className={classes.root}>
        <div className={classes.content}>
          <TransactionForm submitting={submitting} errors={submissionErrors} onSubmit={handleTransactionSubmit} />
          { transactions.length > 0 && <AccoutBalance className={classes.balance} balance={balance} /> }
          <TransactionList transactions={transactions} />
        </div>
    </div>
  );
};


export default App;
