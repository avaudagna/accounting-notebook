import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { red, green } from '@material-ui/core/colors';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import TRANSACTION_TYPES from "../constants";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '80%',
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto'
    },
    titleItem: {
        padding: '8px',
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    debit: {
        background: red[300],
        color: '#FFFFFFFF',
    },
    credit: {
        background: green[300],
        color: '#FFFFFFFF',
    },
    amount: {
        padding: '8px',
        marginLeft: '4px',
        fontSize: theme.typography.pxToRem(18),
        fontWeight: theme.typography.fontWeightRegular,
    },

}));

const TransactionList = (props) => {
    const classes = useStyles();
    const { transactions } = props;
    return (
    <div className={classes.root}>
        { transactions.map((transaction, transactionIndex) => {
            return (
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel-content"
                    >
                    <Typography className={
                        `${classes[transaction.type.toLowerCase()]} ${classes.titleItem}`}>{transaction.type}</Typography>
                    <Typography className={classes.amount}>
                        {transaction.type === TRANSACTION_TYPES.DEBIT && '-'}${transaction.amount}
                    </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <List component="div" disablePadding>
                        {
                            Object.entries(transaction).map(([key, value], itemIndex) => {
                                return (
                                    <ListItemText key={`item-${transactionIndex}-${itemIndex}`} primary={value} secondary={key}/>
                                )
                            })
                        }
                        </List>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            )
        })}
    </div>
    );
}

export default TransactionList;