import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    container: {
        padding: '0px 16px',
        display: 'inline-block',
        margin: 'auto'
    },
    content: {
        display: 'flex'
    },
    balanceLabel: {
        fontSize: '20px'
    },
    balance: {
        fontWeight: 'bold',
        fontSize: '20px',
        alignSelf: 'center'
    }
}));

const AccoutBalance = ({ balance, className }) => {
    const classes = useStyles();

    return <Card className={className}>
        <div className={classes.container}>
            <div className={classes.content}>
                <Typography className={classes.balanceLabel}>The current balance is</Typography>&nbsp;<Typography className={classes.balance}>${balance}</Typography>
            </div>
        </div>
    </Card>
};

export default AccoutBalance;