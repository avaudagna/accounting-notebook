import React, { useState } from 'react';
import TransactionTypes from '../constants.js';
import { red } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto auto 16px auto',
        width: '30%'
    },
    title: {
        textAlign: 'center'
    },
    input: {
        marginBottom: '4px',
        width: '100%'
    },
    buttonHolder:{
        textAlign: 'center'
    },
    error: {
      color: red[500],
      textAlign: 'center'
    },
  }));


const TransactionForm = ({submitting, onSubmit, errors}) => {
    const [type, setType] = useState(TransactionTypes.DEBIT);
    const [amount, setAmount] = useState(0);
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <h2 className={classes.title}>Register new transaction</h2>
            <form onSubmit={(e) => { e.preventDefault(); onSubmit({type: type, amount: amount}); }}>
                <TextField
                    type="text"
                    onChange={(e) => setAmount(e.target.value)}
                    value={amount}
                    variant="filled"
                    name="amount"
                    className={classes.input}
                />
                <Select
                    name="type"
                    variant="filled"
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                    className={classes.input}
                >
                    <MenuItem value={TransactionTypes.DEBIT}>{TransactionTypes.DEBIT}</MenuItem>
                    <MenuItem value={TransactionTypes.CREDIT}>{TransactionTypes.CREDIT}</MenuItem>
                </Select>
                <div className={classes.buttonHolder}>
                    <Button variant="contained" color="primary" type="submit" disabled={submitting} className={classes.button}>Submit</Button>
                </div>
            </form>
            {errors.amount && <p className={classes.error}>{errors.amount}</p>}
        </div>
    );
}

export default TransactionForm;