import axios from "axios"

class AccountService {
    BASE_URL = "http://localhost:8080"

    erros = null;

    getTransactions() {
        return axios.get(`${this.BASE_URL}/transactions`);
    }

    getBalance() {
        return axios.get(`${this.BASE_URL}/`);
    }
    
    submit(transactionData) {
        return axios.post(`${this.BASE_URL}/transactions`, transactionData);
    }
}

export default new AccountService();