package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class Transaction {

    @Id
    @GeneratedValue
    private Long id;
    public enum Type { CREDIT, DEBIT };
    @Enumerated(EnumType.STRING)
    private Type type;
    @JsonFormat(pattern="dd-MM-yy - HH:mm")
    private LocalDateTime effectiveDate;
    private double amount;
    @ManyToOne
    @JsonIgnore
    private Account account;

    public Transaction() {
        super();
    }

    public Transaction(Type type, LocalDateTime effectiveDate, double amount) {
        this.type = type;
        this.effectiveDate = effectiveDate;
        this.amount = amount;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public void setEffectiveDate(LocalDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public LocalDateTime getEffectiveDate() {
        return effectiveDate;
    }

    @JsonIgnore
    public double processedAmount() {
        return this.type.equals(Type.CREDIT) ? amount : amount * (-1);
    }

    public double getAmount() {
        return amount;
    }
}

