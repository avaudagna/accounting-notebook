package com.example.demo.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Account {

    public static final Long BASE_ACCOUNT_ID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(targetEntity=Transaction.class, mappedBy="account", fetch=FetchType.EAGER)
    private List<Transaction> transactions;

    public Account() {
        super();
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public double getBalance() {
        List<Double> amounts = this.getTransactions().stream().map(Transaction::processedAmount).collect(Collectors.toList());
        return amounts.stream().reduce(0.0, Double::sum);
    }

    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
    }
}