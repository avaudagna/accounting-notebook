package com.example.demo.model;

public class InvalidTransactionException extends RuntimeException {
    public InvalidTransactionException() {
        super("The transaction makes the balance negative and cannot be performed");
    }
}
