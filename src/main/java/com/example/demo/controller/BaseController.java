package com.example.demo.controller;

import com.example.demo.model.Account;
import com.example.demo.model.Transaction;
import com.example.demo.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("http://localhost:3000")
public class BaseController {

    @Autowired
    private AccountRepository accountsRepo;

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map home() {
        Account baseAccount = accountsRepo.findById(Account.BASE_ACCOUNT_ID).get();
        return Collections.singletonMap("balance", baseAccount.getBalance());
    }
}
