package com.example.demo.controller;

import com.example.demo.model.Account;
import com.example.demo.model.InvalidTransactionException;
import com.example.demo.model.Transaction;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin("http://localhost:3000")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionsRepo;

    @Autowired
    private AccountRepository accountsRepo;

    @GetMapping(path = "/transactions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> history() {
        List<Transaction> transactions = transactionsRepo.findTransactionsByAccountId(Account.BASE_ACCOUNT_ID);
        Account baseAccount = accountsRepo.findById(Account.BASE_ACCOUNT_ID).get();
        return ResponseEntity.status(HttpStatus.OK).body(Map.of(
                "transactions", transactions,
                "balance", baseAccount.getBalance()));
    }

    @GetMapping("/transactions/{id}")
    public Transaction getTransaction(@PathVariable long id) {
        return transactionsRepo.findById(id).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND, String.format("Transaction with id %s not found", id)));
    }

    @PostMapping("/transactions")
    public ResponseEntity<String> createTransaction(@RequestBody Transaction transaction) {
        Account baseAccount = accountsRepo.findById(Account.BASE_ACCOUNT_ID).get();
        try {
            if (transaction.getAmount() == 0) {
                return new ResponseEntity<>("The amount value must more than 0", HttpStatus.BAD_REQUEST);
            }
            if (transaction.getAmount() < 0) {
                return new ResponseEntity<>("The amount value must be positive", HttpStatus.BAD_REQUEST);
            }
            if ( (baseAccount.getBalance() + transaction.processedAmount()) < 0) {
                return new ResponseEntity<>("The account has insufficient funds to debit such amount", HttpStatus.BAD_REQUEST);
            }
            transaction.setAccount(baseAccount);
            transaction.setEffectiveDate(LocalDateTime.now());
            transactionsRepo.save(transaction);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (InvalidTransactionException ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
