# README #

On one terminal run the backend

* `mvn install`

* `mvn spring-boot:run`

On another terminal run the frontend

* `cd frontend`

* `npm install`

* `npm start`   